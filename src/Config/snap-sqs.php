<?php

return array(
    'snap-sqs' => [
        'driver' => 'sqs',
        'key'    => env('AWS_SQS_KEY'),
        'secret' => env('AWS_SQS_SECRET'),
        'prefix' => env('AWS_SQS_PREFIX'),
        'queue'  => env('AWS_SQS_QUEUE'),
        'region' => env('AWS_SQS_REGION', 'eu-west-1'),
    ],
);
