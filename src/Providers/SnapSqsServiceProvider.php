<?php

namespace Snap\Sqs\Providers;

use Illuminate\Support\ServiceProvider;
use Snap\Sqs\Queue\Connectors\SnapSqsConnector;


class SnapSqsServiceProvider extends ServiceProvider
{
    protected $is_laravel = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/snap-sqs.php' => config_path('snap-sqs.php'),
        ]);

        $manager = $this->app['queue'];

        $manager->addConnector('snap-sqs', function()
        {
            return new SnapSqsConnector;
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Stub
    }

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function provides()
    {
        // Stub
    }
}
